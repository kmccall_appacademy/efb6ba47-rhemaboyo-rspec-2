def reverser(&prc)
  prc.call.split(' ').map(&:reverse).join(' ')
end

def adder(n = 1, &prc)
  prc.call + n
end

def repeater(t = 1, &prc)
  t.times { prc.call }
end
