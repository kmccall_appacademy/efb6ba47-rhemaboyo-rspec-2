require 'byebug'

def measure(t = 1)
  start_time = Time.now
  t.times { yield }
  (Time.now - start_time) / t
end
